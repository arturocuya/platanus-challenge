require 'httparty'
require 'json'
#require 'parallel'
require 'erb'
require 'ruby-progressbar'

BaseUrl = 'https://www.buda.com/api/v2'

markets = JSON.parse(HTTParty.get("#{BaseUrl}/markets").body)['markets']

markets.each {|m| m['max']=[]}

Today = (Time.now.to_f * 1000).to_i # for ms
OneDay = 24 * 60 * 60 * 1000
Yesterday = Today - OneDay

puts 'Doing the thing...'
progress = ProgressBar.create

step = (100 / markets.length()).to_i

ts = Today

#Parallel.each_with_index(markets) do |market, i|
markets.each_with_index do |market, i|
  
  time_limit = OneDay 

  while time_limit >= 0 do
    trades_for_ts = JSON.parse(HTTParty.get("#{BaseUrl}/markets/#{market['id']}/trades?timestamp=#{ts}").body)['trades']

    one_day_trades = trades_for_ts['entries'].select {|e| e[0].to_i > Yesterday }

    if one_day_trades.length() == 0 
      break
    end

    ts_max = one_day_trades.max_by{|e| e[2].to_f }

    if ts_max[2].to_f > markets[i]['max'][2].to_f
     # When I tried using Parallel this seemed to have
     # no effect outside the each_with_index loop
     markets[i]['max'] = ts_max
    end

    last_ts = trades_for_ts['last_timestamp'].to_i
    time_limit -= (ts - last_ts)
  end

  step.times {progress.increment}
end

step.times {progress.increment}

template = File.read(File.expand_path('template.html.erb'))
rhtml = ERB.new(template).result_with_hash({ :markets => markets })
puts rhtml

#File.write('output.html', rhtml)
